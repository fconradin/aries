from django.db import models
from django.conf import settings


class ReviewComment(models.Model):

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='user_reviews_comment',
        null=True,
    )
    review = models.ForeignKey(
        verbose_name="review",
        to="luna.Review",
        on_delete=models.SET_NULL,
        related_name="review_comment",
        null=True,
    )
    content = models.TextField(
        verbose_name="content"
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )

    modified = models.DateTimeField(
        verbose_name='modified',
        auto_now=True,
    )
