from django.contrib.auth import get_user_model
from django.db import models
User = get_user_model()


class User_Extension(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    location = models.TextField(
        verbose_name='location',
        null=True,
        blank=True
    )

    things_i_love = models.TextField(
        verbose_name='things_i_love',
        null=True,
        blank=True
    )

    description = models.TextField(
        verbose_name='description',
        null=True,
        blank=True
    )

    # TODO implement phone filed validation
    phone = models.CharField(
        verbose_name='phone',
        max_length=25,
    )

    # TODO test this approach
    image = models.ImageField(
        verbose_name='image',
    )
