from django.conf import settings
from django.db import models


class ReviewLike(models.Model):

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='user_review_like',
        null=True,
    )
    review = models.ForeignKey(
        verbose_name="like",
        to="luna.Review",
        on_delete=models.SET_NULL,
        related_name="review_like",
        null=True,
    )

    class Meta:
        unique_together = [
            ('user', 'review')
        ]
