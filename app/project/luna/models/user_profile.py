import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models


def code_generator(length=5):
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(length))


User = get_user_model()


class UserProfile(models.Model):
    user = models.OneToOneField(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='user_profile',
    )

    location = models.TextField(
        verbose_name='location',
        null=True,
        blank=True,
    )

    things_i_love = models.TextField(
        verbose_name='things_i_love',
        null=True,
        blank=True,
    )

    description = models.TextField(
        verbose_name='description',
        null=True,
        blank=True,
    )

    # TODO implement phone filed validation
    phone = models.CharField(
        verbose_name='phone',
        max_length=25,
        null=True,
        blank=True,
    )

    # TODO test this approach
    image = models.ImageField(
        verbose_name='image',
        null=True,
        blank=True,

    )

    code = models.CharField(
        verbose_name='code',
        help_text='random code used for registration and for password reset',
        max_length=15,
        default=code_generator
    )

    def generate_new_code(self):
        self.code = code_generator()
        self.save()
        return self.code
