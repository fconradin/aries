from django.conf import settings
from django.db import models


class ReviewCommentLike(models.Model):

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='user_comment_like',
        null=True,
    )
    comment = models.ForeignKey(
        verbose_name="comment",
        to="luna.ReviewComment",
        on_delete=models.SET_NULL,
        related_name="comment_like",
        null=True,
    )

    class Meta:
        unique_together = [
            ('user', 'comment')
        ]
