from django.conf import settings
from django.db import models


class Review(models.Model):
    content = models.TextField(
        verbose_name='content',
    )

    RATING_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )

    rating = models.SmallIntegerField(
        verbose_name='rating',
        choices=RATING_CHOICES,
    )

    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )

    modified = models.DateTimeField(
        verbose_name='modified',
        auto_now=True,
    )

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='user_reviews',
        null=True,
    )

    restaurant = models.ForeignKey(
        verbose_name='restaurant',
        to='luna.Restaurant',
        on_delete=models.SET_NULL,
        related_name='restaurant_reviews',
        null=True,
    )
