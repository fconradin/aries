from django.db import models
from django.conf import settings


class Restaurant(models.Model):
    name = models.CharField(
        verbose_name='name',
        max_length=50,
    )

    CATEGORY_CHOICES = (
        ('Sushi', 'Sushi'),
        ('Pizza', 'Pizza'),
    )

    category = models.CharField(
        verbose_name='category',
        max_length=50,
        choices=CATEGORY_CHOICES,
    )

    COUNTRY_CHOICES = (
        ('CH', 'Switzerland'),
        ('DE', 'Germany'),
        ('IT', 'Italy'),
        ('ES', 'Spain'),
    )

    country = models.CharField(
        verbose_name='coutry',
        choices=COUNTRY_CHOICES,
        max_length=50,
    )

    email = models.EmailField(
        verbose_name='email',
        max_length=100,
    )

    street = models.CharField(
        verbose_name='street',
        max_length=150,
    )

    city = models.CharField(
        verbose_name='city',
        max_length=50,
    )

    website = models.URLField(
        verbose_name='website',
        max_length=100,
        null=True,
        blank=True,
    )

    # TODO implement phone filed validation
    phone = models.CharField(
        verbose_name='phone',
        max_length=25,
        null=True,
        blank=True,
    )

    opening_hours = models.TimeField(
        verbose_name='opening hours',
        null=True,
        blank=True,
    )

    closing_hours = models.TimeField(
        verbose_name='closing hours',
        null=True,
        blank=True,
    )

    PRICE_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
    )

    price_level = models.SmallIntegerField(
        verbose_name='price level',
        choices=PRICE_CHOICES,
        null=True,
        blank=True,
    )

    zip = models.SmallIntegerField(
        verbose_name='ZIP',
    )

    # TODO test this approach
    image = models.ImageField(
        verbose_name='image',
        null=True,
        blank=True,
    )

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='user_restaurants',
        null=True,
    )
