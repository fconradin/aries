from django.contrib import admin

from project.luna.models.comment_like import ReviewCommentLike
from project.luna.models.restaurant import Restaurant
from project.luna.models.review import Review
from project.luna.models.review_comment import ReviewComment
from project.luna.models.review_like import ReviewLike
from project.luna.models.user_profile import UserProfile

admin.site.register(Restaurant)
admin.site.register(Review)
admin.site.register(UserProfile)
admin.site.register(ReviewCommentLike)
admin.site.register(ReviewComment)
admin.site.register(ReviewLike)
