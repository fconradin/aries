from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.core.mail import EmailMessage

from project.luna.models.restaurant import Restaurant

User = get_user_model()


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = '__all__'
        read_only_fields = ['id', 'user']

    def create(self, validated_data):
        return Restaurant.objects.create(
            **validated_data,
            user=self.context.get('request').user,
        )

    def send_email(self, subject, body):
        message = EmailMessage(
            subject=subject,
            body=f'Hey {self.context.get("request").user.username}, {body}',
            to=[self.context.get('request').user.email],
        )
        message.send()
