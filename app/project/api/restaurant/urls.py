from django.urls import path
from .views import RestaurantGetUpdateDeleteView, RestaurantCreateView, RestaurantSearchView, RestaurantByUserView, \
    RestaurantByCategoryView, RestaurantGetView

app_name = 'restaurant'

urlpatterns = [
    path('<int:pk>/', RestaurantGetUpdateDeleteView.as_view(), name='restaurant_detail'),
    path('new/', RestaurantCreateView.as_view(), name='post_create'),
    path('?search=<str:search_string>/', RestaurantSearchView.as_view(), name='restaurant_search'),
    path('user/<int:user_id>/', RestaurantByUserView.as_view(), name='restaurant_by_user'),
    path('category/<str:category>/', RestaurantByCategoryView.as_view(), name='restaurant_by_category'),
    path('', RestaurantGetView.as_view(), name='restaurants'),
]
