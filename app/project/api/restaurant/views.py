from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.views import APIView
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

# from project.api.base import GetObjectMixin
# from project.api.permissions import IsOwnerOrReadOnly
from project.luna.models.restaurant import Restaurant
from .serializers import RestaurantSerializer
from django.db.models import Q


class RestaurantGetUpdateDeleteView(GenericAPIView):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, **kwargs):
        restaurant = self.get_object()
        serializer = self.get_serializer(restaurant)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        restaurant = self.get_object()
        serializer = self.get_serializer(
            restaurant,
            data=request.data,
            context={'request': request},
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        serializer.send_email('Luna Restaurant Update', 'you just updated a restaurant!')
        return Response(serializer.data)

    def delete(self, request, **kwargs):
        restaurant = self.get_object()
        restaurant.delete()
        return Response('OK')


class RestaurantCreateView(GenericAPIView):
    serializer_class = RestaurantSerializer
    permission_classes = []

    def post(self, request):
        serializer = self.get_serializer(
            data=request.data,
            context={'request': request},
        )
        serializer.is_valid(raise_exception=True)
        restaurant = serializer.create(serializer.validated_data)
        serializer.send_email('Luna Restaurant Creation', 'you just created a restaurant!')
        return Response(RestaurantSerializer(restaurant).data)


class RestaurantSearchView(ListAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()
    permission_classes = []

    def filter_queryset(self, queryset):
        search_string = self.request.query_params.get('search')
        if search_string:
            queryset = queryset.filter(
                Q(name__contains=search_string) |
                Q(adress__contains=search_string)
            )
        return queryset


class RestaurantByUserView(GenericAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, user_id):
        restaurants = Restaurant.objects.filter(user_id=user_id)
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)


class RestaurantByCategoryView(GenericAPIView):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, category):
        restaurants = Restaurant.objects.filter(category=category)
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)


class RestaurantGetView(APIView):

    def get(self, request):
        restaurants = Restaurant.objects.all()
        serializer = RestaurantSerializer(restaurants, many=True)
        return Response(serializer.data)
