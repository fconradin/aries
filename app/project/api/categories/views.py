from rest_framework.views import APIView
from rest_framework.response import Response
from project.luna.models import Restaurant


class CategoryView(APIView):
    def get(self, request):
        return Response(Restaurant._meta.get_field("category").choices)
