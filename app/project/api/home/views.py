from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from django.db.models import Sum, Count

from project.api.restaurant.serializers import RestaurantSerializer
from project.luna.models.restaurant import Restaurant


class BestRestaurantsView(GenericAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()

    def get(self, request):
        # should work
        restaurants = self.get_queryset().annotate(
            average_rating=Sum("restaurant_reviews__rating")/Count("restaurant_reviews__rating")
        ).order_by('-average_rating')
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)
