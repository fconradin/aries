from django.urls import path
from .views import BestRestaurantsView

app_name = 'home'

urlpatterns = [
    path('', BestRestaurantsView.as_view(), name='best_restaurants'),
]
