from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class TestGetCurrentUser(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='test_user',
            password='hello_world',
        )

    def authenticate(self):
        self.refresh = RefreshToken.for_user(self.user)
        self.access_token = self.refresh.access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.access_token}')

    def test_can_get_len_one(self):
        self.authenticate()
        url = reverse('api:users:current_user')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.count(), 1)
