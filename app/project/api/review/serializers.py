from django.contrib.auth import get_user_model
from rest_framework import serializers

from project.luna.models.review import Review

User = get_user_model()

# todo


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'
        read_only_fields = ['id', 'user', 'created']
