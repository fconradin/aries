from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.core.mail import EmailMessage

# from project.api.base import GetObjectMixin
# from project.api.permissions import IsOwnerOrReadOnly
from project.luna.models.review import Review
from project.luna.models.review_like import ReviewLike
from project.luna.models.restaurant import Restaurant
from .serializers import ReviewSerializer
# from django.db.models import Q


class ReviewGetUpdateDeleteView(GenericAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, **kwargs):
        review = self.get_object()
        serializer = self.get_serializer(review)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        review = self.get_object()
        serializer = self.get_serializer(review, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def delete(self, request, **kwargs):
        review = self.get_object()
        review.delete()
        return Response('OK')


class ReviewByUserView(GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, user_id):
        reviews = Review.objects.filter(user_id=user_id)
        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)


class ReviewByRestaurantView(GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        # IsAuthenticated,
        # IsOwnerOrReadOnly,
    ]

    def get(self, request, restaurant_id):
        reviews = Review.objects.filter(restaurant_id=restaurant_id)
        serializer = self.get_serializer(reviews, many=True)
        return Response(serializer.data)


class LikeDislikeReviewView(GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [

    ]

    def post(self, request, **kwargs):
        review = self.get_object()
        ReviewLike.objects.get_or_create(
            user=request.user,
            review=review
        )
        message = EmailMessage(
            subject='Review Liked',
            body=f'Hey {review.user.username}, your review got liked!',
            to=[review.user.email],
        )
        message.send()
        return Response('Review liked!')

    def delete(self, request, **kwargs):
        review = self.get_object()
        like = ReviewLike.objects.get(review=review)
        like.delete()
        return Response('Like removed!')


class CurrentUserLikedReviewsView(APIView):
    def get(self, request):
        reviews = Review.objects.filter(review_like__user=request.user)
        serializer = ReviewSerializer(reviews, many=True)
        return Response(serializer.data)


class CurrentUserCommentedReviewsView(APIView):
    def get(self, request):
        reviews = Review.objects.filter(review_comment__user=request.user)
        serializer = ReviewSerializer(reviews, many=True)
        return Response(serializer.data)


# class ReviewCreateView(GenericAPIView):
#     serializer_class = ReviewSerializer
#     permission_classes = []
#
#     def post(self, request, restaurant_id):
#         # restaurant = Restaurant.objects.get(id=restaurant_id)
#         serializer = self.get_serializer(
#             data=request.data,
#             context={'request': request, 'restaurant': restaurant_id},
#         )
#         serializer.is_valid(raise_exception=True)
#         review = serializer.create(serializer.validated_data)
#         return Response(ReviewSerializer(review).data)


class ReviewCreateView(GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Restaurant.objects.all()

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        restaurant = self.get_object()
        Review.objects.create(
            **request.data,
            user=request.user,
            restaurant=restaurant,
        )
        return Response('Review created!')
