from django.urls import path
from .views import ReviewGetUpdateDeleteView, ReviewByUserView, ReviewByRestaurantView, LikeDislikeReviewView, \
    CurrentUserLikedReviewsView, CurrentUserCommentedReviewsView, ReviewCreateView

app_name = 'review'

urlpatterns = [
    path('new_review/<int:pk>/', ReviewCreateView.as_view(), name='new_review'),
    path('<int:pk>/', ReviewGetUpdateDeleteView.as_view(), name='review_detail'),
    path('user/<int:user_id>/', ReviewByUserView.as_view(), name='review_by_user'),
    path('restaurant/<int:restaurant_id>/', ReviewByRestaurantView.as_view(), name='review_by restaurant'),
    path('like/<int:pk>/', LikeDislikeReviewView.as_view(), name='like_dislike_review'),
    path('likes/', CurrentUserLikedReviewsView.as_view(), name='user_likes'),
    path('comments/', CurrentUserCommentedReviewsView.as_view(), name='user_comments'),
]
