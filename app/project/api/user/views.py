from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.user.serializers import UserProfileSerializer
from project.luna.models import UserProfile


class AllUsersView(ListAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    permission_classes = []


class CurrentUserView(APIView):

    def get(self, request, **kwargs):
        user = UserProfile.objects.get(user=request.user)
        serializer = UserProfileSerializer(user)
        return Response(serializer.data)


class SearchUserView(APIView):

    def get(self, request, user_id, **kwargs):
        user = UserProfile.objects.get(user=user_id)
        serializer = UserProfileSerializer(user)
        return Response(serializer.data)
