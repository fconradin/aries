from rest_framework import serializers

from project.luna.models import UserProfile


class UserProfileSerializer (serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = "__all__"
        read_only_fields = (fields,)
