from django.urls import path
from .views import AllUsersView, CurrentUserView, SearchUserView

app_name = 'users'

urlpatterns = [
    path('list/', AllUsersView.as_view(), name='all_users'),
    path('me/', CurrentUserView.as_view(), name='current_user'),
    path('<int:user_id>/', SearchUserView.as_view(), name='search_user_id')
]
