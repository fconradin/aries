from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import ReviewCommentSerializer
from django.core.mail import EmailMessage

from project.luna.models.review_comment import ReviewComment
from project.luna.models.review import Review
from project.luna.models.comment_like import ReviewCommentLike


class CommentsByUserView(APIView):
    def get(self, request, user_id):
        comments = ReviewComment.objects.filter(user_id=user_id)
        serializer = ReviewCommentSerializer(comments, many=True)
        return Response(serializer.data)


class CommentPostDeleteView(GenericAPIView):
    serializer_class = ReviewCommentSerializer
    queryset = Review.objects.all()

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        review = self.get_object()
        ReviewComment.objects.create(
            **request.data,
            user=request.user,
            review=review
        )

        return Response('Comment added!')

    def delete(self, request, **kwargs):
        review = self.get_object()
        comment = ReviewComment.objects.get(review=review)
        comment.delete()
        return Response('Comment removed!')


class LikeDislikeCommentView(GenericAPIView):
    # currently any user is able to remove likes
    serializer_class = ReviewCommentSerializer
    queryset = ReviewComment.objects.all()

    def post(self, request, **kwargs):
        comment = self.get_object()
        ReviewCommentLike.objects.get_or_create(
            user=request.user,
            comment=comment,
        )
        message = EmailMessage(
            subject='Comment Liked',
            body=f'Hey {comment.user.username}, your comment got liked!',
            to=[comment.user.email],
        )
        message.send()
        return Response('Comment liked!')

    def delete(self, request, **kwargs):
        comment = self.get_object()
        like = ReviewCommentLike.objects.get(comment=comment)
        like.delete()
        return Response('Like removed!')
