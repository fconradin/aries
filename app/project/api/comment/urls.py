from django.urls import path
from .views import CommentsByUserView, CommentPostDeleteView, LikeDislikeCommentView

app_name = 'comment'

urlpatterns = [
    path('<int:user_id>/', CommentsByUserView.as_view(), name='new_review'),
    path('new/<int:pk>', CommentPostDeleteView.as_view(), name='add_delete_comment'),  # different from gitlab endpoints
    path('like/<int:pk>', LikeDislikeCommentView.as_view(), name='like_dislike_comment'),
]
