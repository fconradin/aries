from django.contrib.auth import get_user_model
from rest_framework import serializers

from project.luna.models.review_comment import ReviewComment

User = get_user_model()


class ReviewCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewComment
        fields = '__all__'
        read_only_fields = ['id', 'user', 'created']
