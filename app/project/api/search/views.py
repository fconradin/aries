from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth import get_user_model

from project.api.restaurant.serializers import RestaurantSerializer
from project.luna.models.restaurant import Restaurant

from project.api.review.serializers import ReviewSerializer
from project.luna.models.review import Review

from .serializers import UserSerializer

User = get_user_model()


class SearchView(APIView):
    def post(self, request):
        search_string = request.data["search_string"]
        if request.data["type"] == 'restaurants':
            restaurants = Restaurant.objects.all()
            restaurants = restaurants.filter(
                Q(name__contains=search_string) |
                Q(category__contains=search_string) |
                Q(country__contains=search_string) |
                Q(street__contains=search_string) |
                Q(city__contains=search_string)
            )
            serializer = RestaurantSerializer(restaurants, many=True)
            return Response(serializer.data)

        elif request.data["type"] == 'reviews':
            reviews = Review.objects.all()
            reviews = reviews.filter(
                Q(content__contains=search_string)
            )
            serializer = ReviewSerializer(reviews, many=True)
            return Response(serializer.data)

        elif request.data["type"] == 'users':
            users = User.objects.all()
            users = users.filter(
                Q(username__contains=search_string) |
                Q(first_name__contains=search_string) |
                Q(last_name__contains=search_string) |
                Q(email__contains=search_string)
            )
            serializer = UserSerializer(users, many=True)
            return Response(serializer.data)

        else:
            raise Exception('Invalid type!')
