from django.urls import path, include

app_name = 'api'

urlpatterns = [
    path('auth/', include('project.api.auth.urls', namespace='auth')),
    path('restaurants/', include('project.api.restaurant.urls', namespace='restaurant')),
    path('reviews/', include('project.api.review.urls', namespace='review')),
    path('comments/', include('project.api.comment.urls', namespace='comment')),
    path('registration/', include('project.api.registration.urls', namespace='registration')),
    path('users/', include('project.api.user.urls', namespace='user')),
    path('home/', include('project.api.home.urls', namespace='home')),
    path('search/', include('project.api.search.urls', namespace='search')),
    path('category/', include('project.api.categories.urls', namespace='category')),
]
