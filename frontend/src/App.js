import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './routes/Home'
import store from './store'
import Search from './routes/Search';
import Login from './routes/Login';
import { fetchLocalUser } from './store/actions/currentUser';
import Create_Restaurant from "./routes/Create_Restaurant";
import Signup from "./routes/Signup";
import Signup_Thanks from './routes/Signup_Thanks';
import Signup_Validation from './routes/Singnup_Validation';


store.dispatch(fetchLocalUser());

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
          <Router>
            <Switch>
                <Route exact path="/" component={ Home } />
                <Route exact path="/search" component={ Search } />
                <Route exact path="/login" component={ Login } />
                <Route exact path="/restaurant/new/" component={Create_Restaurant} />
                <Route exact path="/signup" component={ Signup } />
                <Route exact path="/signup/thanks" component={ Signup_Thanks } />
                <Route exact path="/signup/validation" component={ Signup_Validation } />
              {/* </Layout> */}
            </Switch>
          </Router>
      </Provider>
    )
  }
}

export default App;
