import React, { Component } from "react";
import "./index.css";

class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "Home"
    };
  }

  handleClick = e => {
    console.log(this.state);
    e.preventDefault();
    this.setState({ activeTab: e.currentTarget.id });
    console.log(this.state);
  };

  render() {
    return (
      <div>
        <div className="footer_upper_container">
          <div className="footer_upper_section">
            <a href="" className="footer_link">
              About Us
            </a>
            <a href="" className="footer_link">
              Press
            </a>
            <a href="" className="footer_link">
              Blog
            </a>
            <a href="" className="footer_link">
              iOS
            </a>
            <a href="" className="footer_link">
              Android
            </a>
          </div>
          <div className="footer_upper_section">
            <img className="footer_image" src="images/facebook.svg" alt="Facebook"/>
            <img className="footer_image" src="images/twitter.svg" alt="Twitter"/>
            <img className="footer_image" src="images/googleplus.svg" alt="GooglePlus"/>
            <img className="footer_image" src="images/instagram.svg" alt="Instagram"/>
          </div>
        </div>
        <div className="footer_lower_container">&copy; Copyright Luna 2018 </div>
      </div>
    );
  }
}

export default Footer;
