import React from "react";
import "./index.css";

const StarsRating = props => (
  <div className="StartsRating_container">
    {[1, 2, 3, 4, 5].map(i => (
      <div
        id={i}
        key={i}
        className={`${i<=props.rating ? "StarsRating_filled" : "StarsRating_empty"}`}
      />
    ))}
  </div>
);

export default StarsRating;
