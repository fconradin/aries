import React, { Component } from "react";
import "./index.css";
import AriesButton from "../AriesButton";


class MainSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "Home"
    };
  }
  render() {
    return (
      <div className="mainSearchBar_container">
        <input type="text" className="MainSearchBar_search" placeholder="Search.." />
        <AriesButton text="Search" roundLeft={true} roundRight={true} large={true}/>
      </div>
    );
  }
}

export default MainSearchBar;
