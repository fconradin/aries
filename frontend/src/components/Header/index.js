import React, { Component } from "react";
import { connect } from "react-redux"; 

import "./index.css";
import AriesButton from "../AriesButton/";
import Heading from "../Heading";
import {withRouter} from 'react-router-dom';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: "Home"
    };
  }

  handleClick = e => {
    console.log(this.state);
    e.preventDefault();
    this.setState({ active: e.currentTarget.id });
    console.log(this.state);
  };

  handleSignup = () => {
    console.log("Header hanldeSignup")
    this.props.history.push("/signup")
  }

  handleLogin = () => {
    console.log(this.props)
    console.log("Header hanldeLogin")
    this.props.history.push("/search")
  }

  render() {
    return (
      <header className="header_container">
        <img className="header_image header_section" src="images/logo.png" alt="Logo"/>
        <div className="header_section">
          {["Home", "Search", "Profile"].map(link => (
            <Heading
              key={link}
              text={link}
              id={link}
              active={this.state.active === link}
              onClick={this.handleClick}
            />
          ))}
          <AriesButton text="LOGIN" roundLeft={true} handleClick={this.handleLogin}/>
          <AriesButton text="SIGNUP" roundRight={true} handleClick={this.handleSignup}/>
        </div>
      </header>
    );
  }
}


export default connect()(withRouter(Header));
