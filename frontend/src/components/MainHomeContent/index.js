import React from "react";
import "./index.css";
import Heading from "../Heading";
import StarsRating from "../StarsRating";

// class MainHomeContent extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       activeTab: "Home"
//     };
//   }
//   render() {
//     return <Heading text="BEST RATED RESTAURANTS" active={true} largeText={true}/>;
//   }
// }

const MainHomeContent = props => (
  <div>
    <Heading text="BEST RATED RESTAURANTS" active={true} largeText={true} />
    <StarsRating rating={3}/>
  </div>
);

export default MainHomeContent;
