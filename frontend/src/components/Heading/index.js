import React from "react";
import "./index.css";
import { Link } from 'react-router-dom';

const Heading = props => (
  <div className="Heading_container">
    <Link
      to={props.text.toLowerCase()}
      className={`
        ${props.active ? "Heading_text_active" : "Heading_text"}
        ${props.largeText ? "Heading_large_text" : undefined}`
      }
    >
      {props.text}
    </Link>
    <div className={props.active ? "Heading_active" : undefined} />
  </div>
);

export default Heading;
