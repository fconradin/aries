import React from "react";
import "./index.css";
import { button } from 'react-router-dom';

const AriesButton = props => (
  <button
    className={`AriesButton_button ${props.roundLeft &&
      "AriesButton_roundLeft"} ${props.roundRight &&
      "AriesButton_roundRight"} ${props.large && "AriesButton_large"}`}
      onClick={props.handleClick}
  >
    {props.text}
  </button>
)

export default AriesButton;
