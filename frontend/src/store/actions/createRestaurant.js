import { devUrl } from "../constants";

export const CREATE_RESTAURANT = "createRestaurant";

export const createRestaurant = data => {
  return {
    type: CREATE_RESTAURANT,
    payload: { data }
  };
};

export const create_restaurant_action = data => dispatch => {
  const token = JSON.parse(localStorage.getItem("access"));

  //   const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90e…iOjF9.yozGTrNjNh-3vZeHgLcBzvsDe0MZC-KmBNgchhHnVaY'
  let myHeaders = new Headers({
    "Content-Type": "application/json"
  });
  myHeaders.set("Authorization", `Bearer ${token}`);

  let config = {
    method: "POST",
    headers: myHeaders,
    body: JSON.stringify(data)
  };
  //   console.log("store actions createRest config", myHeaders);
  //   console.log("store actions createRest config", config);
  fetch(`${devUrl}restaurants/new/`, config)
    .then(response => response.json())
    .then(data => {
      const newRestaurant = createRestaurant(data);
      dispatch(newRestaurant);
    });
};

export const create_restaurant_action_2 = data => dispatch => {
  const token = JSON.parse(localStorage.getItem("access"));

  //   const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90e…iOjF9.yozGTrNjNh-3vZeHgLcBzvsDe0MZC-KmBNgchhHnVaY'
  let myHeaders = new Headers({});
  myHeaders.set("Authorization", `Bearer ${token}`);

  let formData = new FormData();
  for (let name in data) {
    console.log("adding form data", name, data[name])
    formData.append(name, data[name]);
  }

  console.log(formData.values());

  let config = {
    method: "POST",
    headers: myHeaders,
    body: formData
  };

  fetch(`${devUrl}restaurants/new/`, config)
    .then(response => response.json())
    .then(data => {
      const newRestaurant = createRestaurant(data);
      dispatch(newRestaurant);
    });
};
