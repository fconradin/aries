import { STORE_TOKENS } from "../constants";

// import { STORE_TOKENS } from "../constants";

const storeTokens = (access, refresh) => ({
  type: STORE_TOKENS,
  payload: { access, refresh }
});

const fetchLocalUser = () => dispatch => {
  const access = JSON.parse(localStorage.getItem("access"));
  const refresh = JSON.parse(localStorage.getItem("refresh"));
  if (access && refresh) {
    dispatch(storeTokens(access, refresh));
  }
};

export { storeTokens, fetchLocalUser };
