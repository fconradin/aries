import { devUrl } from './constants';
import { storeTokens } from './actions/currentUser';

const fetchRestaurants = () => (dispatch, getState) => {
    // const config = returnFetchConfig(dispatch, getState, pushFn, "GET");
  
    fetch(`${devUrl}restaurants/`)
      .then(res => {
        return res.json();
      })
      .then(restaurants => {
        console.log("fetch restaurants api call", restaurants);
        // dispatch(storeFeed(feed));
      });
  };
  
  const login = (username, password) => (dispatch, getState) => {
    // console.log("store index login")
    const headers = new Headers({ "Content-Type": "application/json" });
    const body = JSON.stringify({ username, password });
    const config = { headers, method: "POST", body };
    return fetch(`${devUrl}auth/token/`, config)
      .then(res => {
        if (res.status === 200) {
          return res.json();
        }
      })
      .then(credentials => {
        if (credentials) {
          console.log("store index login token", credentials);
          const access = JSON.stringify(credentials.access);
          const refresh = JSON.stringify(credentials.refresh);
          localStorage.setItem("access", access);
          localStorage.setItem("refresh", refresh);
          dispatch(storeTokens(credentials.access, credentials.refresh));
          return true;
        }
      });
  };
  
  export { fetchRestaurants, login };