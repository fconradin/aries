import { STORE_TOKENS } from "../constants";

// import { STORE_TOKENS } from './constants';

const currentUser = (state={}, action) => {
    console.log("store reducers currentUser");
    switch (action.type) {
        case STORE_TOKENS:
            return {access: action.payload.access, refresh: action.payload.refresh} 
        default:
            return state;
    }
}

export {currentUser}
