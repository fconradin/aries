import {currentUser} from './currentUser'
import { combineReducers } from 'redux';

const reducer = combineReducers({currentUser})

export {reducer}
