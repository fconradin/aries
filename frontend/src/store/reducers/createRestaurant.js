import {CREATE_RESTAURANT} from "../actions/createRestaurant";

const createRestaurant_reducer = function (state={}, action) {
    switch(action.type){
        case CREATE_RESTAURANT:
            const newState = {...state, ...action.payload}
            console.log(newState)
            return newState
        default: return state
    }
}

export {createRestaurant_reducer};
