import React, { Component } from "react";
import Heading from "../../components/Heading";
import AriesButton from "../../components/AriesButton";

class Signup_Thanks extends Component {
  submit = () => {
    this.props.history.push('/signup/validation/')
  };

  render() {
    return (
      <div>
        <Heading text="REGISTRATION" largeText={true} active={true} />
        <p>
          Thanks for your registration. Our hard working monkeys are preparing a
          digital message called E-Mail that will be sent to you soon. Since
          monkeys arent good in writing the message could end up in you junk
          folder. Our apologies for any inconvienience.
        </p>
        <AriesButton
          large={true}
          text="Finish Signup"
          roundLeft={true}
          roundRight={true}
          handleClick={this.submit}
        />
      </div>
    );
  }
}

export default Signup_Thanks;
