import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import AriesButton from "../../components/AriesButton";
import { create_restaurant_action_2 } from "../../store/actions/createRestaurant";

class Create_Restaurant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      category: "Sushi",
      country: "CH",
      street: "",
      city: "",
      zip: "",
      website: "",
      phone: "",
      email: "",
      opening_hours: "",
      closing_hours: "",
      price_level: "1",
      image: null
    };
  }

  handleChange = e => {
    let obj = {};
    obj[e.currentTarget.id] = e.currentTarget.value;
    this.setState(obj);
    console.log(this.state)
  };

  handleFileChange = e => {
    console.log(e.target.files)
    this.setState({image: e.target.files[0]})
  }

  handleClick = event => {
    event.preventDefault();
    let newObj = {};
    for (const [key, value] of Object.entries(this.state)) {
      newObj[key] = value || null;
    }
    this.props.dispatch(create_restaurant_action_2(newObj));
  };

  render() {
    return (
      <div>
        <h1 className="page_title"> Create New Restaurant </h1>
        <form onSubmit={this.handleClick}>
          <input type="file" onChange={this.handleFileChange}/>
          <input
            value={this.state.name}
            className="input_bar"
            type="text"
            placeholder="Name"
            id="name"
            autoComplete="name"
            onChange={this.handleChange}
          />
          <select
            className="input_bar"
            value={this.state.category}
            onChange={this.handleChange}
            id="category"
          >
            <option defaultValue     value="Sushi">Sushi</option>
            <option value="Pizza">Pizza</option>
          </select>
          <select
            value={this.state.country}
            className="input_bar"
            type="select"
            onChange={this.handleChange}
            id="country"
            autoComplete="country-name"
          >
            <option value="CH">Switzerland</option>
            <option value="IT">Italy</option>
            <option value="DE">Dütschland</option>
            <option value="ES">Spain</option>
          </select>
          <input
            value={this.state.street}
            className="input_bar"
            type="text"
            placeholder="Street"
            onChange={this.handleChange}
            id="street"
            autoComplete="address-line1"
          />
          <input
            value={this.state.city}
            className="input_bar"
            type="text"
            placeholder="City"
            onChange={this.handleChange}
            id="city"
            autoComplete="address-level2"
          />
          <input
            value={this.state.zip}
            className="input_bar"
            type="text"
            placeholder="ZIP"
            onChange={this.handleChange}
            id="zip"
            autoComplete="postal-code"
          />
          <input
            value={this.state.website}
            className="input_bar"
            type="url"
            placeholder="Website"
            onChange={this.handleChange}
            id="website"
          />
          <input
            value={this.state.phone}
            className="input_bar"
            type="text"
            placeholder="Phone"
            onChange={this.handleChange}
            id="phone"
            autoComplete="tel-national"
          />
          <input
            value={this.state.email}
            className="input_bar"
            type="email"
            autoComplete="email"
            placeholder="Email address"
            onChange={this.handleChange}
            id="email"
          />
          <input
            value={this.state.opening_hours}
            className="input_bar"
            type="time"
            placeholder="Opening Hours"
            onChange={this.handleChange}
            id="opening_hours"
          />
          <input
            value={this.state.closing_hours}
            className="input_bar"
            type="time"
            placeholder="Closing Hours"
            onChange={this.handleChange}
            id="closing_hours"
          />
          <select
            className="input_bar"
            value={this.state.price_level}
            onChange={this.handleChange}
            id="price_level"
            type="number"
          >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
          </select>
          <br />
          <AriesButton
            text="Create New"
            roundRight={true}
            roundLeft={true}
            large={true}
            onClick={this.handleClick}
          />
        </form>
      </div>
    );
  }
}

export default connect()(Create_Restaurant);
