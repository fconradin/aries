import React from "react";
import "./index.css";
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import MainSearchBar from "../../components/MainSearchBar";
import MainHomeContent from "../../components/MainHomeContent";

const Home = () => (
      <div className="Site">
        <Header/>
        <MainSearchBar/>
        <MainHomeContent/>
        <Footer />
      </div>
    );

export default Home;
