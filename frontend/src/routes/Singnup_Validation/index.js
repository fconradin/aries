import React, { Component } from "react";
import Heading from "../../components/Heading";
import AriesButton from "../../components/AriesButton";
import "./index.css";
import { devUrl } from "../../store/constants";

class Signup_Validation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      code: "",
      first_name: "",
      last_name: "",
      password: "",
      password_repeat: "",
      passwords_match: true
    };
  }
  submit = e => {
    e.preventDefault();
    let passwords_match = this.state.password === this.state.password_repeat;
    this.setState({ passwords_match });
    if (passwords_match) {
      const headers = new Headers({ "Content-Type": "application/json" });
      const body = JSON.stringify(this.state);
      const config = { headers, method: "POST", body };
      console.log(config);
      fetch(`${devUrl}registration/validation/`, config).then(res => {
          console.log(res)
        if (res.status === 200) {
          this.props.history.push("/login");
        } else {
            alert("Something went wrong");
        }
      });
    }
  };

  handleChange = e => {
    let obj = {};
    obj[e.currentTarget.id] = e.currentTarget.value;
    this.setState(obj);
  };

  render() {
    return (
      <div>
        <Heading text="VALIDATION" largeText={true} active={true} />
        <form onSubmit={this.handleSubmit}>
          <input
            type="email"
            id="email"
            placeholder="Email address"
            value={this.state.email}
            onChange={this.handleChange}
            autoComplete="email"
          />
          <input
            type="code"
            id="code"
            placeholder="Validation Code"
            value={this.state.code}
            onChange={this.handleChange}
            autoComplete="code"
          />
          <input
            type="text"
            id="first_name"
            placeholder="First name"
            value={this.state.first_name}
            onChange={this.handleChange}
            autoComplete="first_name"
          />
          <input
            type="text"
            id="last_name"
            placeholder="Last name"
            value={this.state.last_name}
            onChange={this.handleChange}
            autoComplete="last_name"
          />
          <input
            type="password"
            id="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChange}
            autoComplete="password"
            className={
              this.state.passwords_match
                ? undefined
                : "Signup_Validation_no_match"
            }
          />
          <input
            type="password"
            id="password_repeat"
            placeholder="Repeat password "
            value={this.state.password_repeat}
            onChange={this.handleChange}
            autoComplete="password"
            className={
              this.state.passwords_match
                ? undefined
                : "Signup_Validation_no_match"
            }
          />
          <AriesButton
            large={true}
            text="Finish Signup"
            roundLeft={true}
            roundRight={true}
            handleClick={this.submit}
          />
        </form>
      </div>
    );
  }
}

export default Signup_Validation;
