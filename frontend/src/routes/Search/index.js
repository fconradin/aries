import React, {Component} from "react";
import { connect } from "react-redux";

import "./index.css";
// import StarsRating from "../../components/StarsRating";
import { fetchRestaurants } from "../../store/apiCalls";
// import fetchRestaurants from "../../store"

class Search extends Component {
  handleClick = () => {
    console.log("handing Click");
    this.props.dispatch(fetchRestaurants);
  };
  render() {
    return <button onClick={this.handleClick}>click</button>;
  }
}

export default connect()(Search);
