import React, { Component } from "react";
import { connect } from "react-redux";

import "./index.css";
import AriesButton from "../../components/AriesButton";
import Heading from "../../components/Heading";
import { devUrl } from "../../store/constants";
// import StarsRating from "../../components/StarsRating";
// import { fetchRestaurants } from "../../store/apiCalls";
// import fetchRestaurants from "../../store"

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "" };
  }
  //Todo what if it fails --> implement this
  submitRegistration = () => {
    const headers = new Headers({ "Content-Type": "application/json" });
    const body = JSON.stringify({ email: this.state.email });
    const config = { headers, method: "POST", body };
    fetch(`${devUrl}registration/`, config)
    this.props.history.push('/signup/thanks')
  };

  handleChange = (e) => {
    this.setState({email:e.currentTarget.value})
  };

  render() {
    return (
      <div>
        <Heading text="REGISTRATION" largeText={true} active={true} />
        <input
          type="email"
          placeholder="Email address"
          onChange={this.handleChange}
          value={this.state.email}
        />
        <AriesButton
          large={true}
          text="Register"
          roundLeft={true}
          roundRight={true}
          handleClick={this.submitRegistration}
        />
      </div>
    );
  }
}

export default connect()(Signup);
