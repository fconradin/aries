import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { login } from "../../store/apiCalls";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "flurin",
      password: "rabbatueff",
      wrongCredentials: false
    };
  }

  componentDidMount() {
    this.checkUser();
  }

  checkUser() {
    if (this.props.access) {
      this.props.history.push("/");
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props
      .dispatch(login(this.state.username, this.state.password))
      .then(data => {
        if (data) {
          this.props.history.push("/");
          this.setState({ username: "", password: "" });
        } else this.setState({ wrongCredentials: true });
      });
  };
  handlePassword = e => {
    this.setState({ password: e.currentTarget.value });
  };
  handleEmail = e => {
    this.setState({ username: e.currentTarget.value });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          className={
            this.state.wrongCredentials ? "Login_wrongCredentials" : undefined
          }
          type="username"
          placeholder="Email address"
          value={this.state.username}
          onChange={this.handleEmail}
          autoComplete="username"
        />
        <input
          className={
            this.state.wrongCredentials ? "Login_wrongCredentials" : undefined
          }
          type="password"
          placeholder="password"
          value={this.state.password}
          onChange={this.handlePassword}
          autoComplete="password"
        />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    access: state.currentUser.access
  };
}

export default connect(mapStateToProps)(Login);
